# Building on linux
The initial release was not linux ready, this repo attempts to fix that...

# Run
sudo apt install unar libgtk-3-dev autoconf cmake
cd libs
for f in *; do unar $f; done

# Build the 3rd party libraries
cd boost*
./bootstrap.sh
./b2
./b2 headers

cd ../db*/build_unix
../dist/configure
make
sudo make install

cd ../../openssl*
./Configure
make
sudo make install

cd ../wx*
./autogen.sh
./configure
make
sudo make install

# Building
cd bitcoin0.1/src
mkdir build
cmake ../.
cmake --build .